CTBE Programming for Bioinformatics Study Group

* Programming languages: Python (version 3), R

* Version control distributed system: Git

*

A) OBJECTIVES OF THE GROUP

1. 

2. 

B) ORGANIZATION OF THE MEETINGS

There will be two types of meetings:

1. Development meeting:

Challenges in Bioinformatics (e.g. sequence assembly) will be discussed.

2. Project meeting:

Problems that usually appear in projects involving Bioinformatic analyses (e.g. building an histogram to show distribution of contig lengths in genome assembly projects) will be discussed.
