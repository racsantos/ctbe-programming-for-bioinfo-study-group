#!/usr/bin/python

import argparse
import itertools
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.Alphabet import generic_dna

parser = argparse.ArgumentParser(description='Lists all possible k-mers occuring in a given sequence', add_help=True)
parser.add_argument('--inputNuclFasta', dest='nuclFasta', metavar='nucleotides.fasta', type=str, help='FASTA sequence', required=True)
parser.add_argument('--K', dest='kmerSize', metavar='K-mer size', type=int, help='Integer corresponding to the k-mer size', required=True)

args = parser.parse_args()

ksize = args.kmerSize

countKmers=[]
Kmers=[]

bases = ['A','T','G','C']

for p in itertools.product(bases, repeat=ksize):
	mer = ''.join(p)
	#print(mer)
	Kmers = Kmers + [mer]
	genomeOBJ = open(args.nuclFasta, "rU")
	for Seq in SeqIO.parse(genomeOBJ, "fasta"):
		countKmers = countKmers + [Seq.seq.count(mer)]
		#print(Seq.seq.count(mer))
	genomeOBJ.close()

print(countKmers)
#print(Kmers)
