#!/usr/bin/python

#########################################
#
# Script that converts an input DNA sequence to an amino acid sequence
# Author: Renato Augusto Correa dos Santos
#
#

""" Sequence used as input
>gi|44238768|gb|AY536639.1| Aspergillus niger xylanase (XYNB) mRNA, complete cds
ATGCTCACCAAGAACCTTCTCCTCTGCTTCGCCGCAGCTAAGGCTGTTCTGGCCGTTCCCCACGACTCTG
TCGTCGAGCGTTCGGATGCCTTGCACAAGCTCTCTGAGCGTTCGACCCCGAGCTCGACCGGCGAGAACAA
CGGCTTCTACTACTCCTTCTGGACCGACGGCGGTGGTGATGTGACCTACACCAACGGTGACGCTGGCTCG
TACACCGTTGAGTGGTCTAACGTTGGCAACTTTGTTGGTGGAAAGGGCTGGAACCCTGGAAGTGCGCAGG
ACATTACCTACAGCGGCACCTTCACCCCTAGCGGCAACGGCTACCTCTCCGTCTATGGCTGGACCACTGA
CCCCCTGATCGAGTACTACATCGTCGAGTCCTACGGCGACTACAACCCCGGCAGTGGAGGCACCTACAAG
GGCACCGTCACCTCCGATGGATCCGTCTACGATATCTACACGGCTACCCGCACCAACGCCGCTTCCATCC
AAGGAACCGCTACCTTCACCCAGTACTGGTCCGTTCGCCAGAACAAGAGAGTCGGAGGAACTGTTACCAC
TTCCAACCACTTCAACGCTTGGGCTAAGCTGGGCATGAACCTGGGTACTCACAACTACCAGATCGTGGCT
ACCGAGGGCTACCAGAGCAGCGGATCTTCGTCCATCACTGTTCAGTAA

"""

import sys

# Function to convert from DNA to RNA sequence
def convertDNA2RNA(DNAseq):
	RNAseq = ''
	for DNAnucl in DNAseq:
		DNAnuclUpper = DNAnucl.upper()
		if DNAnuclUpper == "A":
			RNAnucl = "A"
			RNAseq = RNAseq + RNAnucl 
		if DNAnuclUpper == "T":
			RNAnucl = "U"
			RNAseq = RNAseq + RNAnucl
		if DNAnuclUpper == "C":
			RNAnucl = "C"
			RNAseq = RNAseq + RNAnucl
		if DNAnuclUpper == "G":
			RNAnucl = "G"
			RNAseq = RNAseq + RNAnucl
	return RNAseq

# Function that gets information from input sequence and print on the screen
def getInputSeqInfo(DNAseq):
        firstCodon = DNAseq[0:3]
	if firstCodon == "ATG":
        	print("First codon: %s (correct reading frame)" % firstCodon)
	else:
		print("There is something wrong with your CDS sequence. The first codon (%s) is not ATG (standard genetic code)" % firstCodon)
		exit()
        return

def convertRNA2AminoAcids(RNAseq):
	countAA = ((len(RNAseq)) / 3)
	print("Length: %d (RNA sequence) and %d (amino acids sequence)" % (len(RNAseq), countAA))
	firstIndexCodon = 0
	thirdIndexCodon = 3
	PROTEINseq = ''
	while thirdIndexCodon < len(RNAseq):
		thirdIndexCodon = firstIndexCodon + 3
		codon = RNAseq[firstIndexCodon:thirdIndexCodon]
		firstIndexCodon = firstIndexCodon + 3
		#if () or () or :
		if codon == "AUG":
			PROTEINseq = PROTEINseq + "M"
		elif (codon == "UUU") or (codon == "UUC"):
			PROTEINseq = PROTEINseq + "F"
		elif (codon == "UUA") or (codon == "UUG") or (codon == "CUU") or (codon == "CUC") or (codon == "CUA") or (codon == "CUG"):
			PROTEINseq = PROTEINseq + "L"
		elif (codon == "AUU") or (codon == "AUC") or (codon == "AUA"):
			PROTEINseq = PROTEINseq + "I"
		elif (codon == "GUU") or (codon == "GUC") or (codon == "GUA") or (codon == "GUG"):
			PROTEINseq = PROTEINseq + "V"
		elif (codon == "UCU") or (codon == "UCC") or (codon == "UCA") or (codon == "UCG") or (codon == "AGU") or (codon =="AGC"):
			PROTEINseq = PROTEINseq + "S"
		elif (codon == "CCU") or (codon == "CCC") or (codon == "CCA") or (codon == "CCG"):
			PROTEINseq = PROTEINseq + "P"
		elif (codon == "ACU") or (codon == "ACC") or (codon == "ACA") or (codon == "ACG"):
			PROTEINseq = PROTEINseq + "T"
		elif (codon == "GCU") or (codon == "GCC") or (codon == "GCA") or (codon == "GCG"):
			PROTEINseq = PROTEINseq + "A"
		elif (codon == "UAU") or (codon == "UAC"):
			PROTEINseq = PROTEINseq + "Y"
		elif (codon == "CAU") or (codon == "CAC"):
			PROTEINseq = PROTEINseq + "H"
		elif (codon == "CAA") or (codon == "CAG"):
			PROTEINseq = PROTEINseq + "Q"
		elif (codon == "AAU") or (codon == "AAC"):
			PROTEINseq = PROTEINseq + "N"
		elif (codon == "AAA") or (codon == "AAG"):
			PROTEINseq = PROTEINseq + "K"
		elif (codon == "GAU") or (codon == "GAC"):
			PROTEINseq = PROTEINseq + "D"
		elif (codon == "GAA") or (codon == "GAG"):
			PROTEINseq = PROTEINseq + "E"
		elif (codon == "UGU") or (codon == "UGC"):
			PROTEINseq = PROTEINseq + "C"
		elif codon == "UGG":
			PROTEINseq = PROTEINseq + "W"
		elif (codon == "CGU") or (codon == "CGC") or (codon == "CGA") or (codon == "CGG") or (codon == "AGG") or (codon == "AGA"):
			PROTEINseq = PROTEINseq + "R"
		elif (codon == "GGU") or (codon == "GGC") or (codon == "GGA") or (codon == "GGG"):
			PROTEINseq = PROTEINseq + "G"
		elif (codon == "UAA") or (codon == "UGA") or (codon == "UAG"):
			PROTEINseq = PROTEINseq + "*"
		else:
			print("Something wrong with your code (codon %s)! Check the possible codons or the genetic code of the organism you are working with" % codon),
			sys.exit()
	return PROTEINseq

inputDNA = "ATGCTCACCAAGAACCTTCTCCTCTGCTTCGCCGCAGCTAAGGCTGTTCTGGCCGTTCCCCACGACTCTGTCGTCGAGCGTTCGGATGCCTTGCACAAGCTCTCTGAGCGTTCGACCCCGAGCTCGACCGGCGAGAACAACGGCTTCTACTACTCCTTCTGGACCGACGGCGGTGGTGATGTGACCTACACCAACGGTGACGCTGGCTCGTACACCGTTGAGTGGTCTAACGTTGGCAACTTTGTTGGTGGAAAGGGCTGGAACCCTGGAAGTGCGCAGGACATTACCTACAGCGGCACCTTCACCCCTAGCGGCAACGGCTACCTCTCCGTCTATGGCTGGACCACTGACCCCCTGATCGAGTACTACATCGTCGAGTCCTACGGCGACTACAACCCCGGCAGTGGAGGCACCTACAAGGGCACCGTCACCTCCGATGGATCCGTCTACGATATCTACACGGCTACCCGCACCAACGCCGCTTCCATCCAAGGAACCGCTACCTTCACCCAGTACTGGTCCGTTCGCCAGAACAAGAGAGTCGGAGGAACTGTTACCACTTCCAACCACTTCAACGCTTGGGCTAAGCTGGGCATGAACCTGGGTACTCACAACTACCAGATCGTGGCTACCGAGGGCTACCAGAGCAGCGGATCTTCGTCCATCACTGTTCAGTAA"

getInputSeqInfo(inputDNA)
RNAsequence = convertDNA2RNA(inputDNA)
Protein = convertRNA2AminoAcids(RNAsequence)

print(inputDNA)
print(RNAsequence)
print(Protein)
