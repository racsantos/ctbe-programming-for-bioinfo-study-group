#!/usr/bin/python

# Importing modules in Python
import math

# Declaring variables in Python
a = 3
b = 4
c = 7

# Some math operations
sumResult = a + b
subtractionResult = c - a
multiplicationResult = c * b
divisionResult = c / b
divisionFloorResult = c // b
negationResult = -c
absolutValueResult = abs(negationResult)
squareRootResult = math.sqrt(9)
moduloResult = 7%3

# Printing some variables
print(sumResult)
print(subtractionResult)
print(multiplicationResult)
print(divisionResult)
print(divisionFloorResult)
print(negationResult)
print(absolutValueResult)
print(squareRootResult)
print(moduloResult)
