#!/usr/bin/python

import sys
from itertools import takewhile

filename = sys.argv[1]

codontable = {
'ACC':'T', 'ACG':'T', 'ACT':'T', 'ACA':'T',
'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
'ATT':'I', 'ATC':'I', 'ATA':'I', 'CTA':'L', 
'CTC':'L', 'CTG':'L', 'CTT':'L', 'CCA':'P', 
'CCC':'P', 'CCG':'P', 'CCT':'P',
'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
'TAC':'Y', 'TAT':'Y', 'TAA':'*', 'TAG':'*',
'TGC':'C', 'TGT':'C', 'TGA':'*', 'TGG':'W', 'ATG':'M'
}

stop_codons = ('TAA', 'TGA', 'TAG')

def translate_dna(sequence, codontable, stop_codons):       
    start = sequence.find('ATG')
    # Take sequence from the first start codon
    trimmed_sequence = sequence[start:]
    # Split it into triplets
    codons = [trimmed_sequence[i:i+3] for i in range(0, len(trimmed_sequence), 3)]
    print(len(codons))
    print(trimmed_sequence)
    print(codons)
    # Take all codons until first stop codon
    coding_sequence  =  takewhile(lambda x: x not in stop_codons and len(x) == 3 , codons)
    # Translate and join into string
    protein_sequence = ''.join([codontable[codon] for codon in coding_sequence])
    print(protein_sequence)
    # This line assumes there is always stop codon in the sequence
    return "{0}_".format(protein_sequence)
   
header = ''
sequence = ''
for line in open(filename):
    if line[0] == ">":
            if header != '':
                    print header
                    translate_dna(sequence, codontable, stop_codons)
            header = line.strip()
            sequence = ''
    else:
            sequence += line.strip()
print header
translate_dna(sequence, codontable, stop_codons)
